local srcpath = core.get_modpath("terms_of_service") .. "/src"

aes_tos = {}

dofile(srcpath .. "/player_manager.lua")
dofile(srcpath .. "/privs.lua")
dofile(srcpath .. "/rules.lua")
dofile(srcpath .. "/tos.lua")