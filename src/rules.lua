local S = core.get_translator("terms_of_service")
local FS = function(...) return core.formspec_escape(S(...)) end

local function get_fs() end



function aes_tos.show_rules(p_name)
  core.show_formspec(p_name, "tos:rules", get_fs())
end





----------------------------------------------
---------------FUNZIONI LOCALI----------------
----------------------------------------------

function get_fs()
  local fs = {
    "formspec_version[4]",
    "size[12,17.3]",
    "no_prepend[]",
    "background[0,0;12,17.3;tos_rules_bg.png]",
    "bgcolor[;true]",
    "hypertext[1,0.1;10,2;weap_txt;<global size=24 halign=center valign=middle font=mono><style color=#302c2e><b>" .. FS("SERVER RULES") .. "</b>]",
    "hypertext[3.5,3.65;7.1,1.2;pname_txt;<global size=14 font=mono valign=middle color=#302c2e><b>" .. FS("Always respect other players") .. "</b>]",
    "hypertext[3.5,7.13;7.1,1.2;pname_txt;<global size=14 font=mono valign=middle color=#302c2e><b>" .. FS("No excessive swearing") .. "</b>]",
    "hypertext[3.5,10.85;7.1,1.2;pname_txt;<global size=14 font=mono valign=middle color=#302c2e><b>" .. FS("No cheating") .. "</b>]",
    "hypertext[1.5,14.05;7.1,1.2;pname_txt;<global size=14 font=mono valign=middle color=#302c2e><b>" .. FS("This contract has been officially sealed with your soul, a goat and a fidget spinner") .. "</b>]",
    "image_button_exit[12.3,0.19;0.65,0.65;arenalib_infobox_quit.png;quit;;true;false;]"
  }

  return table.concat(fs)
end
