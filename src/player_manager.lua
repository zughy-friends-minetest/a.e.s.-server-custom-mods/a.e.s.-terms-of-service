core.register_on_joinplayer(function(player)
	local p_name = player:get_player_name()

	if not core.get_player_privs(p_name).tos_accepted then
		core.after(1, function()
			aes_tos.show_tos(p_name)
		end)
	end
end)
