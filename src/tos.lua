local S = core.get_translator("terms_of_service")

local function get_fs() end

local TOS =
    "1) " .. S("Always respect other players") .. "\n" ..
    "2) " .. S("No excessive swearing") .. "\n" ..
    "3) " .. S("No cheating") .. "\n\n" ..

    S("Admins reserve the right to remove whoever transgress the rules. In case of repeated misbehaviours, the user will be banned") .. "\n\n" ..
    S("Also, we encourage parents to not leave their children alone if they are younger than 12, as there are elements in the server that staff can't control (the chat)")


function aes_tos.show_tos(p_name)
  core.show_formspec(p_name, "tos:tos_fs", get_fs())
end





----------------------------------------------
---------------FUNZIONI LOCALI----------------
----------------------------------------------

function get_fs()
	local formspec = {
    "formspec_version[4]",
    "size[9,8]",
    "no_prepend[]",
    "bgcolor[;true]",
    "background[0,0;9,8;tos_bg.png]",
    "hypertext[0.5,0.5;8,0.6;TOS_title;<global halign=center><style size=20 font=mono>" .. S("SERVER RULES") .. "]",
	  "hypertext[0.5,1.2;8,5;TOS;"..TOS.."]",
    "container[0.5,6.9]",
	  "label[0,-0.2;" .. S("When can I insult a user? One word") .. "]",
	  "field[0,0;4.5,0.7;entry;;]",
	  "button_exit[4.4,0;1.7,0.7;ok;" .. S("Accept") .. "]",
	  "button[6.33,0;1.7,0.7;no;" .. S("Deny") .. "]",
    "container_end[]",
	  "field_close_on_enter[;false]"
	}
	return table.concat(formspec, "")
end





----------------------------------------------
---------------GESTIONE CAMPI-----------------
----------------------------------------------

core.register_on_player_receive_fields(function(player, formname, fields)
	if formname ~= "tos:tos_fs" then return end

	local p_name = player:get_player_name()

	if fields.ok or fields.key_enter then
		local answers = string.split(core.get_translated_string(core.get_player_information(p_name).lang_code, S("never")), "|")
		local correct = false

		for _, answer in ipairs(answers) do
			if string.lower(fields.entry) == answer then
				correct = true
			end
		end

    if correct then
			local privs = core.get_player_privs(p_name)
			privs.tos_accepted = true
			privs.interact = true
			core.set_player_privs(p_name, privs)
			core.chat_send_player(p_name, S("Enjoy your staying!"))
			core.after(0.1, function()
				serverguide.get_firststeps_formspec(p_name)
			end)
		else
			core.chat_send_player(p_name, S("Wrong, try again"))
			core.after(0.1, function()
			  core.show_formspec(p_name, "tos:tos_fs", get_fs())
			end)
			return
		end

	elseif fields.quit then
			if not core.get_player_privs(p_name).tos_accepted then
				core.chat_send_player(p_name, S("Please read carefully and click 'accept'"))
				core.after(0.1, function()
					core.show_formspec(p_name, "tos:tos_fs", get_fs())
				end)
				return
			end

	elseif fields.no then
		core.kick_player(p_name, S("All the players must adhere to the rules"))
		return

	else
		core.show_formspec(p_name, "tos:tos_fs", get_fs())
	end
end)